#! /usr/bin/python3.5
"""
Email extractor

Exctracts emails from strings stored in text files. WIP for piping in data
using linux pipes
"""
import re
import sys

SUFFIX = 'emails.txt'
EMAIL = re.compile(r'(\w|\.)*(@)(\w*)\.(\w\w)(\w|\.\w\w)') #Create extratcion pattern
emails = []

# Begin extraction process
with open(sys.argv[1], 'r') as text:
    itr = iter(text)
    for i in itr:
        try:
            found = EMAIL.search(i).group()
            emails.append(found)
            print(found)
        except AttributeError:
            pass

# Begin transcription process
name_pattern = re.compile(r'(\w*)(.txt)$')
email_file_name = name_pattern.search(sys.argv[1]).group(1) + '-' + SUFFIX
print('file created:', email_file_name)
with open(email_file_name, 'w') as f:
    return_str = 'Emails found: \n'
    f.write(return_str + '\n'.join(emails))