#!/usr/bin/python3
"""Creates a deranged (see mathematical derangement) secret santa for \'n\' number of people.
This script will create text files in the directory it is being run from which will contain the
names of the people who are sending presents."""
import sys
from subprocess import call
from random import shuffle

people_playing = int(input("How many people playing? "))
player_list = [i for i in range(people_playing)]
i = len(player_list)
print('Type q to quit')
SHUFFLED = False
cmd = ''
debug = False
try:
    if sys.argv[1] == 'd':
        debug = True
except IndexError:
    pass

while True:
    if cmd == 'q':
        sys.exit(1)
    elif i > 0:
        cmd = input('Enter player {!s} name: '.format(len(player_list)-i+1))
        player_list[i-1] = cmd
        i -= 1
    elif not SHUFFLED:
        shuffle(player_list)
        SHUFFLED = True
    else:
        reciever_list = player_list[1:] + [player_list[0]]
        input('Press enter to begin..')
        for i, a in enumerate(player_list):
            cmd = input('{0} press Enter to see who you will be gifting'
                        .format(a))
            instruction = '{0} will be gifting {1}'.format(a, reciever_list[i])
            print(instruction)
            next_player = player_list[i+1] if i+1 < len(player_list) else 'no-one'
            if not debug:
                with open(a+'.txt', 'w') as file:
                        file.write(instruction)
            input('Press Enter and get {} to come to the terminal'.format(next_player))
            call('clear', shell=True)
        break
