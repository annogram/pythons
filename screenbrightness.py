#!/usr/bin/python3.5
"""Little script to set my screen to preffered nightnime brightness"""
import subprocess
import sys

cmd = 'xrandr --output eDP1 --brightness'

try:
    brightness = sys.argv[1]
    subprocess.call(cmd + ' ' + brightness, shell=True)
except IndexError:
    subprocess.call(cmd + ' 1', shell=True)
