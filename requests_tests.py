#!/usr/bin/python3.5
import requests
from bs4 import BeautifulSoup as bs

r = requests.get('http://scp-wiki.net/')
r = requests.utils.get_unicode_from_response(r)
s = bs(r, 'lxml')
print(s.find(id='page-content'))