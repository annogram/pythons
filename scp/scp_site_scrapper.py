#! /usr/bin/python3.5
"""
Made for the SCP community (but mostly for myself)
By ZacharyCallahan
"""
import urllib
import re
import sys
import os

from bs4 import BeautifulSoup

class SCPContentExtractor:
    """
    This class will create an SCPContentExtractor object which is used to grab
    files from the SCP foundation website. http://scp-wiki.net/ and save them in
    markdown format.
    """

    SCP_HOME = 'http://www.scp-wiki.net/'

    def __init__(self):
        """
        Initilization
        """
        self.scpno = ''
        self.page = ''

    def extract(self, scp_no=2):
        """
        Performs the extraction of an scp
        """
        # try:
        self.scpno = self.append_zeros(scp_no)
        self.page = self.SCP_HOME + 'SCP-' + self.scpno # make the string value acceptable
        print('extracting SCP-'+self.scpno)
        soup = self.__extract__(self.page)
        if soup is not None:
            log_tags = soup.find_all('a') # begin extracting logs
            log_links = []
            for tag in log_tags:
                if re.match(r'.*javascript.*', tag['href']) is not None:
                    continue
                elif re.match(r'^(http://)', tag['href']) is None:
                    log_links.append(self.SCP_HOME[:-1] + tag['href'])
                else:
                    log_links.append(tag['href'])
            self.__get_logs__(tuple(log_links))
            self.__convert_to_markdown__(soup, self.scpno)

    def __extract__(self, htmlpage):
        """
        Performs the extraction of a specific content stores an array of soups
        """
        try:
            response = urllib.request.urlopen(htmlpage) # grab the raw response
            soup = BeautifulSoup(response.read(), 'lxml') # soupify
            soup.find('div', 'page-rate-widget-box').clear()
            page_content = soup.find(id='page-content') # find the scp-section of the webpage
            self.__remove_collapsed_areas__(page_content) # remove collapsable blocks
            self.__localize_images__(page_content)
            return page_content
        except AttributeError:
            print('error extracting '+htmlpage)
            with open('error log.txt', 'a') as log:
                log.writelines([htmlpage, '\t'])
        except urllib.error.HTTPError:
            print('Could not find page')


    def __get_logs__(self, log_links):
        """
        Grab the experiment logs from the page
        """
        logdir = os.getcwd() + '/experiment-logs/' + 'SCP-' + self.scpno + '/'
        for log in log_links:
            print('extracting', log)
            soup = self.__extract__(log)
            logpath = logdir + log.split('/')[-1] + '.md'
            # print(os.getcwd() + '/' + log.split('/')[-1] + '.md')
            print('LOG NAME:', log.split('/')[-1])
            try:
                if os.path.isfile(logpath) or soup is None:
                    pass
                elif (os.path.isfile(os.getcwd() + '/' + log.split('/')[-1] + '.md')
                      or re.match(r'(.*)(([sS])([cC])([pP])(-\d\d\d)(\d)?)$',
                                  log.split('/')[-1]) is not None):
                    print('SCP document reference')
                else:
                    if not os.path.isdir(logdir):
                        os.makedirs(logdir)
                    with open(logpath, 'w') as fp:
                        fp.write(str(soup))
            except urllib.error.URLError:
                # javascript ref
                print('error parsing:', log)
    def __remove_collapsed_areas__(self, soup):
        """
        Scp documents normally have collapsable areas emulating "restricted content",
        this does not work well with markdown, therefore i will extract the div blocks in
        these DOM nodes and remove the collapsable regions
        """
        collapasble = soup.find_all('div', re.compile(r'(collapsible-block-)(.*)'))
        for block in collapasble:
            if block['class'][0] == 'collapsible-block-content':
                continue
            elif block['class'][0] == 'collapsible-block-unfolded':
                del block['style']
            else:
                block.clear()

    def __localize_images__(self, page):
        """
        Downloads the images and changes the tag to reference the image
        """
        rspath = os.getcwd() + '/resource/'
        if not os.path.isdir(rspath): # make the resource folder if not already made
            try:
                os.mkdir(rspath)
            except FileExistsError:
                pass 
        img_tags = page.find_all('img') #get all images from the soup
        for img in img_tags:
            imgpath = rspath + self.scpno +'-'+img['src'].split('/')[-1]
            urllib.request.urlretrieve(img['src'], imgpath) #download the image and move it to resources
            img['src'] = imgpath # change the tag to the new value


    @classmethod
    def append_zeros(cls, scp_no):
        """
        Helper method which appends zeros to the front of a number which returns
        as a string, for use with urls and saving files
        """
        append = ''
        if scp_no < 1000:
            zeros = 3 - len(str(scp_no))
            for _ in range(zeros):
                append += '0'
            append += str(scp_no)
        else:
            append = str(scp_no)
        return append

    @classmethod
    def __convert_to_markdown__(cls, page, page_no):
        """
        Converts the content passed into it into markdown
        """
        with open('scp-'+page_no+'.md', 'w') as sf: # savefile (sf)
            sf.write(str(page.prettify()))

if __name__ == '__main__':
    name, begin, end, forks = sys.argv
    #convert
    begin, end, forks = int(begin), int(end), int(forks)
    ALLOC = (end - begin)//forks # I like hole numbers
    pid = 0
    current = None
    for alloc_mut in range(0, forks+1): # fork and allocate
        if pid == 0:
            current = begin + ALLOC*alloc_mut if pid == 0 else begin
            pid = os.fork()
        else:
            ce = SCPContentExtractor()
            for article_no in range(current, current + ALLOC):
                # print('scp no', article_no)
                ce.extract(article_no)
            break #only the master needs to do the outer for loop

    # ce = SCPContentExtractor()
    # for i in range(821,2999):
    #     print ('extracting scp-' + ce.append_zeros(i))
    #     ce.extract(i)
    # ce.extract(75)