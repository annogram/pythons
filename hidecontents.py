#!/usr/bin/python3.5
"""Hides all elements in a directory in a Linux os"""
import os
import sys

_current_dir = sys.argv[1]
operation = sys.argv[2]
invalid = False

for fname in os.listdir(_current_dir):
    if operation == "show":
        if fname[0] == '.':
            os.rename(_current_dir + '/' + fname, _current_dir + '/' + fname[1:])
    elif operation == "hide":
        if fname[0] != '.':
            os.rename(_current_dir + '/' + fname, _current_dir + '/' + '.'+fname)
    else:
        invalid = True
if invalid:
    print("What?")
